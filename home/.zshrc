#  ██████╗ ██████╗ ██╗  ██╗██╗      
#  ██╔══██╗██╔══██╗██║  ██║██║       
#  ██████╔╝██████╔╝███████║██║       
#  ██╔══██╗██╔═══╝ ██╔══██║██║       
#  ██║  ██║██║     ██║  ██║███████╗  
#  ╚═╝  ╚═╝╚═╝     ╚═╝  ╚═╝╚══════╝  
#  - Zshrc 2020 / rphl -


# If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATH

# Path to your oh-my-zsh installation.
export ZSH="/home/rphl/.oh-my-zsh"

# Set name of the theme to load #ZSH_THEME="robbyrussell"
ZSH_THEME="rphl"

# Set list of themes to pick from when loading at random
# ZSH_THEME_RANDOM_CANDIDATES=( "robbyrussell" "agnoster" )

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
HIST_STAMPS="dd.mm.yyyy"

# Set fzf installation directory path
export FZF_BASE=/usr/share/fzf

# Standard plugins can be found in $ZSH/plugins/
# Custom plugins may be added to $ZSH_CUSTOM/plugins/
plugins=(
    fzf
	git
	history-substring-search
	colored-man-pages
	zsh-autosuggestions
	zsh-syntax-highlighting
    zsh-z
)

source $ZSH/oh-my-zsh.sh

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
  export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# PERSONNAL CONFIG
PATH=$PATH:/home/rphl/bin

## ARCHIVE EXTRACTION
# usage: ex <file>
ex ()
{
  if [ -f $1 ] ; then
    case $1 in
      *.tar.bz2)   tar xjf $1   ;;
      *.tar.gz)    tar xzf $1   ;;
      *.bz2)       bunzip2 $1   ;;
      *.rar)       unrar x $1   ;;
      *.gz)        gunzip $1    ;;
      *.tar)       tar xf $1    ;;
      *.tbz2)      tar xjf $1   ;;
      *.tgz)       tar xzf $1   ;;
      *.zip)       unzip $1     ;;
      *.Z)         uncompress $1;;
      *.7z)        7z x $1      ;;
      *.deb)       ar x $1      ;;
      *.tar.xz)    tar xf $1    ;;
      *.tar.zst)   unzstd $1    ;;      
      *)           echo "'$1' cannot be extracted via ex()" ;;
    esac
  else
    echo "'$1' is not a valid file"
  fi
}

## ALIASES ##

# Changing "ls" to "exa"
alias ls='exa -al --color=always --group-directories-first' # my preferred listing
alias la='exa -a --color=always --group-directories-first'  # all files and dirs
alias ll='exa -l --color=always --group-directories-first'  # long format
alias lt='exa -aT --color=always --group-directories-first' # tree listing
alias l.='exa -a | egrep "^\."'

# Journal errors
alias journal="journalctl -p 3 -xb"

# Network
alias network='ip add && sudo nmap -sn 192.168.1.49/24'
alias ip='ip add | grep "inet 192.168"'
alias scan='nmap -sL 192.168.1.0/24 | grep --color -e 'MAC Address' -e '^''
alias ping='ping -c 5 google.fr'
alias top='htop'

# Dotfiles
alias ck='vim ~/.conkyrc'
alias zs='vim ~/.zshrc'
alias doted="vim ~/bin/dotfiles_xfce2020.sh"
alias dot="sh ~/bin/dotfiles_xfce2020.sh"
alias sw="vim ~/.config/sway/config"
alias wb="vim ~/.config/waybar/config"
alias note="vim ~/Documents/Notes/index.md"

# Tools
alias v="vim"
alias f='fzf --exact' 
alias fh='fzf-history-widget' # fzf command history 
alias fv='vim $(fzf --exact)' # fzf search + vim edit
alias push="git add . && git commit && git push"
alias col="sh ~/bin/color-scripts/color-blocks1"
alias light='redshift -o'
alias hello='sh ~/bin/ufetch/ufetch-fedora'
alias neo='neofetch'

# Admin
function coffre {
    encfs ~/.coffre ~/coffre;
    thunar ~/coffre &
}

function stop {
    fusermount -u ~/coffre;
    rm -rf ~/coffre &
}

# FZF config
export FZF_DEFAULT_COMMAND="fd . $HOME --hidden"
export FZF_DEFAULT_OPTS="--layout=reverse --inline-info --height=80%"
