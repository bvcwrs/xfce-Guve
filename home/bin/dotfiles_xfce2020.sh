#!/bin/bash

# FILES
echo "-- GITLAB PROJECT 'XFCE2020' --"
read -p "[1/2] Copy config files ? (y/n): " repcopy
if [ "$repcopy" = "y" ]
then
    #files
	cp ~/.conkyrc ~/Public/Xfce2020/home/.conkyrc
	cp ~/.zshrc ~/Public/Xfce2020/home/.zshrc
	cp ~/.config/neofetch/config.conf ~/Public/Xfce2020/home/.config/neofetch/config.conf
	cp ~/.vimrc ~/Public/Xfce2020/home/.vimrc
	cp ~/.config/rofi/* ~/Public/Xfce2020/home/.config/rofi/
	cp ~/.config/ranger/rc.conf ~/Public/Xfce2020/home/.config/ranger/rc.conf
	cp ~/.oh-my-zsh/themes/rphl.zsh-theme ~/Public/Xfce2020/home/.oh-my-zsh/themes/rphl.zsh-theme
    #folders
	cp -avr ~/bin/* ~/Public/Xfce2020/home/bin/
	cp -avr ~/.local/share/xed/styles/ ~/Public/Xfce2020/home/.local/
	cp -avr ~/Public/Extras/* ~/Public/Xfce2020/Extras/
	xfconf-query -c xfce4-keyboard-shortcuts -lv > ~/Public/Xfce2020/home/keyboard_shortcuts.txt
	sleep 1s
else
	exit
fi

# COMMIT / PUSH
read -p "[2/2] Push to Git ? (y/n): " reppush
if [ "$reppush" = "y" ]
then
	read -r -p "Message:" gitdesc
	cd ~/Public/Xfce2020/ ; git add . ; git commit -m "$gitdesc" ; git push -u origin master
	sleep 1s
else
    exit
fi
